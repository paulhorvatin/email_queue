﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace EmailQueue {
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config) {
            // Web API configuration and services
            ConfigureJson(config);
            // Web API routes
            config.MapHttpAttributeRoutes();
            // CORS settings
            // Error Handling 
        }
        private static void ConfigureJson(HttpConfiguration config) {
            config.Formatters.Clear();
            var jsonMediaTypeFormatter = new JsonMediaTypeFormatter {
                SerializerSettings =
                    new JsonSerializerSettings {
                        ContractResolver = new CamelCasePropertyNamesContractResolver(),
                        Converters = new List<JsonConverter>
                        {
                            new StringEnumConverter {CamelCaseText = true}
                        },
                        DateFormatHandling = DateFormatHandling.IsoDateFormat,
                        DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                        Formatting = Formatting.Indented,
                        NullValueHandling = NullValueHandling.Ignore
                    }
            };
            jsonMediaTypeFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
            config.Formatters.Add(jsonMediaTypeFormatter);
        }
    }
}
