using EmailQueue;
using Swashbuckle.Application;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using WebActivatorEx;
[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace EmailQueue {
    public class SwaggerConfig {
        protected static string GetXmlCommentsPath() {
            return String.Format(@"{0}\bin\MandrillSendService.XML",
              AppDomain.CurrentDomain.BaseDirectory);
        }

        public static void Register() {
            GlobalConfiguration.Configuration
                .EnableSwagger(c => {
                    c.SingleApiVersion("v1", ConfigurationManager.AppSettings["SwaggerTitle"])
                        .Description("An API for accessing the Mandrill Send Service.");
                    c.Schemes(GetSchemesFromAppConfig());
                    c.IgnoreObsoleteActions();
                    //                    c.IncludeXmlComments(GetXmlCommentsPath());
                    c.IgnoreObsoleteProperties();
                    c.UseFullTypeNameInSchemaIds();
                    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                    c.RootUrl(
                        (request) => {
                            //return "http://petstore.swagger.wordnik.com/api/api-docs#";

                            var host = request.Headers.Host;
                            string vdir = request.GetConfiguration().VirtualPathRoot.TrimEnd('/');
                            string baseUrl = string.Format("{0}://{1}{2}",
                                host.Contains("frommfamily") ? "https" : request.RequestUri.Scheme, host, vdir);
                            return baseUrl;
                        });
                })
            .EnableSwaggerUi(c => {
                c.DisableValidator();
                c.DocExpansion(DocExpansion.List);
            });
        }

        private static IEnumerable<string> GetSchemesFromAppConfig() {
            var schemes = new List<string>();
            if (ConfigurationManager.AppSettings["SwaggerHttpsEnable"] == "true") {
                schemes.Add("https");
            }
            else {
                schemes.Add("http");
            }
            return schemes;
        }
    }
}
