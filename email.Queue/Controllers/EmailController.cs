﻿using EmailQueue.Models;
using EmailQueue.Services;
using EntityFramework.Extensions;
using NLog;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using EmailQueue.SQLFunctions;

namespace EmailQueue.Controllers {
    [BasicAuthenticationAttribute]
    public class EmailController : ApiController {
        EmailContext db;
        Logger logger;
        private readonly EmailSendService _emailSendServiceRepo;
        DBFunctions dbFunctions;
        public EmailController() {
            db = new EmailContext();
            logger = NLog.LogManager.GetCurrentClassLogger();
            _emailSendServiceRepo = new EmailSendService(ConfigurationManager.AppSettings["emailSendEndpointBase"]);
            dbFunctions = new DBFunctions(ConfigurationManager.ConnectionStrings["EmailContext"].ToString());
        }
        [SwaggerResponse(HttpStatusCode.OK, "OK", typeof(int))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The request is invalid.")]
        [Route("AddToQueue")]
        [HttpPost]
        public async Task<HttpResponseMessage> AddToQueue(Email email) {
            logger.Info("Adding email to Queue: " + email.ToString());
            var result = db.Emails.Add(email);
            db.SaveChanges();
            logger.Info("Added ID #: " + result.ID);
            return Request.CreateResponse(HttpStatusCode.OK, result.ID);
        }

        [SwaggerResponse(HttpStatusCode.OK, "OK", typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The request is invalid.")]
        [Route("SendEmails")]
        [HttpPost]
        public async Task<HttpResponseMessage> SendEmails(int maxSends = 100) {
            var listOfEmails = db.Emails.Where(x => (x.Status_ID == null || x.Status_ID == 1) && (x.SendAfter == null || x.SendAfter < DateTime.Now)).OrderBy(x => x.SendAfter).Take(maxSends).Select(x => x.ID).ToList();
            logger.Info("Got list of emails: " + listOfEmails.Count);
            //Mark as sending so no duplicates
            if (listOfEmails.Any()) {

                db.Emails.Where(x => listOfEmails.Contains(x.ID)).Update(x => new Email() { Status_ID = 2 });
                try {
                    foreach (var emailId in listOfEmails) {
                        var email = db.Emails.Where(x => x.ID == emailId).FirstOrDefault();
                        if (email != null) {
                            _emailSendServiceRepo.SendMailAsync(email);
                            //HostingEnvironment.QueueBackgroundWorkItem(ct => _emailSendServiceRepo.SendMailAsync(email));
                            logger.Info("Sent Email! ID: " + email.ID + " to " + email.ToAddress);
                        }
                        else {
                            logger.Info("Email ID: " + emailId + " is null");
                        }
                    }
                }
                catch (Exception ex) {
                    Exception e = ex;
                    while (e != null) {
                        logger.Info("Exception: " + e.ToString());
                        e = ex.InnerException;
                    }
                }
                finally {
                    //update all emails
                    db.Emails.Where(x => listOfEmails.Contains(x.ID)).Update(x => new Email() { Status_ID = 3, SentOn = DateTime.Now });
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Sent " + listOfEmails.Count + " emails to send.");
        }

        [SwaggerResponse(HttpStatusCode.OK, "OK", typeof(int))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The request is invalid.")]
        [Route("AddStoreRepEmail")]
        [HttpGet]
        public async Task<HttpResponseMessage> AddStoreRepEmail(Guid StoreId, Guid RepId, int minutesDelayed = 30) {
            logger.Info("Adding Store Rep Email to Queue: " + StoreId + " | " + RepId);
            var model = new StoreRepQueuedEmail(StoreId, RepId, minutesDelayed);
            logger.Info("Created model");

            var existingModel = db.StoreRepQueuedEmails.Where(x => x.StoreId == StoreId && x.RepId == RepId && x.SentToQueueOn == null);
            if (!existingModel.Any()) {
                var result = db.StoreRepQueuedEmails.Add(model);
                logger.Info("added to collection");
                db.SaveChanges();
                logger.Info("SavedChanges");
                logger.Info("Added ID #: " + result.ID);
                return Request.CreateResponse(HttpStatusCode.OK, result.ID);
            }
            logger.Info("Already in Queue. Prevented Duplicate");
            return Request.CreateResponse(HttpStatusCode.OK, -1);
        }

        [SwaggerResponse(HttpStatusCode.OK, "OK", typeof(bool))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The request is invalid.")]
        [Route("InsertStoreAddedByTerritoryRepEmail")]
        [HttpGet]
        public async Task<HttpResponseMessage> InsertStoreAddedByTerritoryRepEmail(Guid StoreId, string toEmail, int minutesDelayed = 30) {
            try {
                dbFunctions.InsertStoreAddedByTerritoryRepEmail(StoreId, toEmail, minutesDelayed);
                logger.Info("Pushed StoreAddedByTerritoryRepEmail to Queue: " + StoreId + " " + toEmail);
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception ex) {
                Exception e = ex;
                while (e != null) {
                    logger.Info("Exception: " + e.ToString());
                    e = ex.InnerException;
                }
                return Request.CreateResponse(HttpStatusCode.OK, false);

            }
        }

        [SwaggerResponse(HttpStatusCode.OK, "OK", typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The request is invalid.")]
        [Route("AddStoreRepObjectEmailsQueue")]
        [HttpPost]
        public async Task<HttpResponseMessage> AddStoreRepObjectEmailsQueue() {
            logger.Info("Getting list of store rep emails where send after < " + DateTime.Now.ToString());
            var now = DateTime.Now;
            var listOfStoreRepLinks = db.StoreRepQueuedEmails.Where(x => (x.SentToQueueOn == null && (x.SendAfter == null || (DateTime.Compare((DateTime)x.SendAfter, now) <= 0)))).Select(x => x.ID).ToList();
            logger.Info("Got list of store rep emails: " + listOfStoreRepLinks.Count);
            //Mark as sending so no duplicates
            if (listOfStoreRepLinks.Any()) {

                db.StoreRepQueuedEmails.Where(x => listOfStoreRepLinks.Contains(x.ID)).Update(x => new StoreRepQueuedEmail() { SentToQueueOn = DateTime.Now });
                try {
                    foreach (var storeRepId in listOfStoreRepLinks) {
                        dbFunctions.InsertStoreRepEmail(storeRepId);
                        logger.Info("Pushed Store Rep Email to Queue: " + storeRepId);
                    }
                }
                catch (Exception ex) {
                    Exception e = ex;
                    while (e != null) {
                        logger.Info("Exception: " + e.ToString());
                        e = ex.InnerException;
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Pushed " + listOfStoreRepLinks.Count + " Store Rep Emails to Queue.");
        }

        [SwaggerResponse(HttpStatusCode.OK, "OK", typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "The request is invalid.")]
        [Route("Insert643ReportEmail")]
        [HttpPost]
        public async Task<HttpResponseMessage> Insert643ReportEmail() {
            logger.Info("Starting to send 643 report");
            //Mark as sending so no duplicates
            try {
                dbFunctions.Insert643ReportEmail();

            }
            catch (Exception ex) {
                Exception e = ex;
                while (e != null) {
                    logger.Info("Exception: " + e.ToString());
                    e = ex.InnerException;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Finished sending 643 report");
        }
    }
}
