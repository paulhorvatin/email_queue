﻿using EmailQueue.Models;
using System.Data.Entity;

namespace EmailQueue {
    public class EmailContext : DbContext {
        public EmailContext() : base() {

        }

        public DbSet<EmailQueue.Models.Email> Emails { get; set; }
        public DbSet<EmailOrigin> EmailOrigins { get; set; }
        public DbSet<EmailRecipientType> EmailRecipientTypes { get; set; }
        public DbSet<EmailStatus> EmailStatuses { get; set; }
        public DbSet<EmailType> EmailTypes { get; set; }
        public DbSet<StoreRepQueuedEmail> StoreRepQueuedEmails { get; set; }
    }
}