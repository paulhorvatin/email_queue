﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.Filters;

namespace EmailQueue {
    public class BasicAuthenticationAttribute : AuthorizationFilterAttribute {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext) {
            if (actionContext.Request.Headers.Authorization == null) {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            else {
                // Gets header parameters  
                string authenticationString = actionContext.Request.Headers.Authorization.Scheme;

                // Validate username and password  
                if (!SimpleValidation.Validate(authenticationString)) {
                    // returns unauthorized error  
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }

            base.OnAuthorization(actionContext);
        }
    }
}