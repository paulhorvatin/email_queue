﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace EmailQueue.SQLFunctions {
    public class DBFunctions {
        string connString;
        public DBFunctions(string Connection) {
            this.connString = Connection;

        }

        public bool InsertStoreRepEmail(int ID) {
            using (SqlConnection con = new SqlConnection(connString)) {
                using (SqlCommand cmd = new SqlCommand("InsertStoreRepEmail", con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                    try {
                        con.Open();
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                    catch (Exception ex) {
                        throw new Exception("Execption inserting store rep email. " + ex.Message);
                    }
                }
            }
        }

        public bool Insert643ReportEmail() {
            using (SqlConnection con = new SqlConnection(connString)) {
                using (SqlCommand cmd = new SqlCommand("Insert643ReportEmail", con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    try {
                        con.Open();
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                    catch (Exception ex) {
                        throw new Exception("Execption Insert643ReportEmail. " + ex.Message);
                    }
                }
            }
        }


        internal bool InsertStoreAddedByTerritoryRepEmail(Guid storeId, string toEmail, int minutesDelayed) {
            using (SqlConnection con = new SqlConnection(connString)) {
                using (SqlCommand cmd = new SqlCommand("InsertStoreAddedByTerritoryRepEmail", con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@storeId", SqlDbType.UniqueIdentifier).Value = storeId;
                    cmd.Parameters.Add("@toEmail", SqlDbType.Int).Value = toEmail;
                    cmd.Parameters.Add("@minutesDelayed", SqlDbType.Int).Value = minutesDelayed;
                    try {
                        con.Open();
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                    catch (Exception ex) {
                        throw new Exception("Execption InsertStoreAddedByTerritoryRepEmail. " + ex.Message);
                    }
                }
            }
        }
    }
}