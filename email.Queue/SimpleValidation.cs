﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace EmailQueue {
    public static class SimpleValidation {
        public static bool Validate(string key) {
            var apiKey = ConfigurationManager.AppSettings["apiKey"];
            if (string.Compare(key, apiKey, true) == 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}