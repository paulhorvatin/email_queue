﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EmailQueue.Models {
    public class BaseObject {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
    }
}