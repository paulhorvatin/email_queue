﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmailQueue.Models {

    public class Email : BaseObject {
        [ForeignKey("Type")]
        public int? Type_ID { get; set; }
        public virtual EmailType Type { get; set; }
        [ForeignKey("Origin")]
        public int? Origin_ID { get; set; }
        public virtual EmailOrigin Origin { get; set; }
        [ForeignKey("Status")]
        public  int? Status_ID { get; set; }
        public virtual EmailStatus Status { get; set; }
        [ForeignKey("RecipientType")]
        public int? RecipientType_ID { get; set; }
        public virtual EmailRecipientType RecipientType { get; set; }
        public int? RecipientID { get; set; }
        public string ToAddress { get; set; }
        public string FromAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int? Template_ID { get; set; }
        public string TemplateData { get; set; }
        public DateTime? SendAfter { get; set; }
        public DateTime? SentOn { get; set; }

        public override string ToString() {
            return "Sending to: " + ToAddress + " | From: " + FromAddress + " | Subject: " + Subject + " | EmailType: " + Type_ID + " | Send After: " + ((SendAfter == null) ? " NOT SET" : ((DateTime)SendAfter).ToString("g")) + " | Status: " + Status_ID + " | Sent On: " + ((SentOn == null) ? " NOT SET" : ((DateTime)SentOn).ToString("g"));
        }
    }
}