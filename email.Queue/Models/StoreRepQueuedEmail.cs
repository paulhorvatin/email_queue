﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmailQueue.Models {

    public class StoreRepQueuedEmail : BaseObject {

        public StoreRepQueuedEmail() { }
        public StoreRepQueuedEmail(Guid storeId, Guid repId, int minutesDelayed) {
            StoreId = storeId;
            RepId = repId;
            SendAfter = DateTime.Now.AddMinutes(minutesDelayed);
        }
        public Guid StoreId { get; set; }
        public Guid RepId { get; set; }
        public DateTime? SendAfter { get; set; }
        public DateTime? SentToQueueOn { get; set; }

    }
}