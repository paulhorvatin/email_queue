﻿using EmailQueue.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace EmailQueue.Services {
    public class EmailSendService {
        public string BaseUri { get; set; }
        public EmailSendService(string baseUrl) {
            this.BaseUri = baseUrl;
        }

        public async void SendMailAsync(Email email) {
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(BaseUri);

                    // HTTP POST
                    var postTask = client.PostAsJsonAsync("Send/SendQueuedEmail", email);
                    postTask.Wait();
                    var result = postTask.Result;

                    if (result.IsSuccessStatusCode) {
                        var jsonString = result.Content.ReadAsStringAsync();
                        jsonString.Wait();
                        var responseModel = JsonConvert.DeserializeObject<bool>(jsonString.Result);
                    }
                    else {
                        throw new Exception((int)result.StatusCode + "-" + result.StatusCode.ToString());
                    }
                }
            }
            catch (Exception e) {
            }
        }

        public void SendQueuedEmailDontWait(Email email) {
            HostingEnvironment.QueueBackgroundWorkItem(ct => SendMailAsync(email));
        }
    }

}