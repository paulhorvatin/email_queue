namespace EmailQueue.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EmailQueue.EmailContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EmailQueue.EmailContext context)
        {
            var emailTypes = new List<EmailType> {
                new EmailType { Name = "Html" },
                new EmailType { Name = "Text-Only" },
                new EmailType { Name = "Template" }

            };
            emailTypes.ForEach(s => context.EmailTypes.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            var emailOrigins = new List<EmailOrigin> {
                new EmailOrigin { Name = "admin.frommfamily.com" },
                new EmailOrigin { Name = "frommfamily.com" },
                new EmailOrigin { Name = "gofromm.com" }

            };
            emailOrigins.ForEach(s => context.EmailOrigins.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            var emailStatuses = new List<EmailStatus> {
                new EmailStatus { Name = "Pending Send" },
                new EmailStatus { Name = "Sending" },
                new EmailStatus { Name = "Sent" },
                new EmailStatus { Name = "Failed" }

            };
            emailStatuses.ForEach(s => context.EmailStatuses.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            var emailRecipientTypes = new List<EmailRecipientType> {
                new EmailRecipientType { Name = "Consumer" },
                new EmailRecipientType { Name = "Retailer" },
                new EmailRecipientType { Name = "Distributor" },
                new EmailRecipientType { Name = "RegionalManager" },
                new EmailRecipientType { Name = "Administrator" },
                new EmailRecipientType { Name = "System" }


            };
            emailRecipientTypes.ForEach(s => context.EmailRecipientTypes.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();
        }
    }
}
