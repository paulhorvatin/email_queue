namespace EmailQueue.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixRelationships : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailOrigins",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmailRecipientTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Type_ID = c.Int(),
                        Origin_ID = c.Int(),
                        Status_ID = c.Int(),
                        RecipientType_ID = c.Int(),
                        RecipientID = c.Int(),
                        ToAddress = c.String(),
                        FromAddress = c.String(),
                        Subject = c.String(),
                        Body = c.String(),
                        Template_ID = c.Int(),
                        TemplateData = c.String(),
                        SendAfter = c.DateTime(),
                        SentOn = c.DateTime(),
                        Origin_ID1 = c.Int(),
                        RecipientType_ID1 = c.Int(),
                        Status_ID1 = c.Int(),
                        Type_ID1 = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EmailOrigins", t => t.Origin_ID1)
                .ForeignKey("dbo.EmailRecipientTypes", t => t.RecipientType_ID1)
                .ForeignKey("dbo.EmailStatus", t => t.Status_ID1)
                .ForeignKey("dbo.EmailTypes", t => t.Type_ID1)
                .Index(t => t.Origin_ID1)
                .Index(t => t.RecipientType_ID1)
                .Index(t => t.Status_ID1)
                .Index(t => t.Type_ID1);
            
            CreateTable(
                "dbo.EmailStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmailTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Emails", "Type_ID1", "dbo.EmailTypes");
            DropForeignKey("dbo.Emails", "Status_ID1", "dbo.EmailStatus");
            DropForeignKey("dbo.Emails", "RecipientType_ID1", "dbo.EmailRecipientTypes");
            DropForeignKey("dbo.Emails", "Origin_ID1", "dbo.EmailOrigins");
            DropIndex("dbo.Emails", new[] { "Type_ID1" });
            DropIndex("dbo.Emails", new[] { "Status_ID1" });
            DropIndex("dbo.Emails", new[] { "RecipientType_ID1" });
            DropIndex("dbo.Emails", new[] { "Origin_ID1" });
            DropTable("dbo.EmailTypes");
            DropTable("dbo.EmailStatus");
            DropTable("dbo.Emails");
            DropTable("dbo.EmailRecipientTypes");
            DropTable("dbo.EmailOrigins");
        }
    }
}
