namespace EmailQueue.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCF : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailOrigins",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmailRecipientTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RecipientID = c.Int(),
                        ToAddress = c.String(),
                        FromAddress = c.String(),
                        Subject = c.String(),
                        Body = c.String(),
                        Template_ID = c.Int(),
                        TemplateData = c.String(),
                        SendAfter = c.DateTime(),
                        SentOn = c.DateTime(),
                        Origin_ID = c.Int(),
                        RecipientType_ID = c.Int(),
                        Status_ID = c.Int(),
                        Type_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EmailOrigins", t => t.Origin_ID)
                .ForeignKey("dbo.EmailRecipientTypes", t => t.RecipientType_ID)
                .ForeignKey("dbo.EmailStatus", t => t.Status_ID)
                .ForeignKey("dbo.EmailTypes", t => t.Type_ID)
                .Index(t => t.Origin_ID)
                .Index(t => t.RecipientType_ID)
                .Index(t => t.Status_ID)
                .Index(t => t.Type_ID);
            
            CreateTable(
                "dbo.EmailStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmailTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Emails", "Type_ID", "dbo.EmailTypes");
            DropForeignKey("dbo.Emails", "Status_ID", "dbo.EmailStatus");
            DropForeignKey("dbo.Emails", "RecipientType_ID", "dbo.EmailRecipientTypes");
            DropForeignKey("dbo.Emails", "Origin_ID", "dbo.EmailOrigins");
            DropIndex("dbo.Emails", new[] { "Type_ID" });
            DropIndex("dbo.Emails", new[] { "Status_ID" });
            DropIndex("dbo.Emails", new[] { "RecipientType_ID" });
            DropIndex("dbo.Emails", new[] { "Origin_ID" });
            DropTable("dbo.EmailTypes");
            DropTable("dbo.EmailStatus");
            DropTable("dbo.Emails");
            DropTable("dbo.EmailRecipientTypes");
            DropTable("dbo.EmailOrigins");
        }
    }
}
