namespace EmailQueue.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedRelationships2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Emails", "Origin_ID");
            DropColumn("dbo.Emails", "RecipientType_ID");
            DropColumn("dbo.Emails", "Status_ID");
            DropColumn("dbo.Emails", "Type_ID");
            RenameColumn(table: "dbo.Emails", name: "Origin_ID1", newName: "Origin_ID");
            RenameColumn(table: "dbo.Emails", name: "RecipientType_ID1", newName: "RecipientType_ID");
            RenameColumn(table: "dbo.Emails", name: "Status_ID1", newName: "Status_ID");
            RenameColumn(table: "dbo.Emails", name: "Type_ID1", newName: "Type_ID");
            RenameIndex(table: "dbo.Emails", name: "IX_Type_ID1", newName: "IX_Type_ID");
            RenameIndex(table: "dbo.Emails", name: "IX_Origin_ID1", newName: "IX_Origin_ID");
            RenameIndex(table: "dbo.Emails", name: "IX_Status_ID1", newName: "IX_Status_ID");
            RenameIndex(table: "dbo.Emails", name: "IX_RecipientType_ID1", newName: "IX_RecipientType_ID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Emails", name: "IX_RecipientType_ID", newName: "IX_RecipientType_ID1");
            RenameIndex(table: "dbo.Emails", name: "IX_Status_ID", newName: "IX_Status_ID1");
            RenameIndex(table: "dbo.Emails", name: "IX_Origin_ID", newName: "IX_Origin_ID1");
            RenameIndex(table: "dbo.Emails", name: "IX_Type_ID", newName: "IX_Type_ID1");
            RenameColumn(table: "dbo.Emails", name: "Type_ID", newName: "Type_ID1");
            RenameColumn(table: "dbo.Emails", name: "Status_ID", newName: "Status_ID1");
            RenameColumn(table: "dbo.Emails", name: "RecipientType_ID", newName: "RecipientType_ID1");
            RenameColumn(table: "dbo.Emails", name: "Origin_ID", newName: "Origin_ID1");
            AddColumn("dbo.Emails", "Type_ID", c => c.Int());
            AddColumn("dbo.Emails", "Status_ID", c => c.Int());
            AddColumn("dbo.Emails", "RecipientType_ID", c => c.Int());
            AddColumn("dbo.Emails", "Origin_ID", c => c.Int());
        }
    }
}
