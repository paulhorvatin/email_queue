namespace EmailQueue.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StoreRepEMail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StoreRepQueuedEmails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StoreId = c.Guid(nullable: false),
                        RepId = c.Guid(nullable: false),
                        SendAfter = c.DateTime(),
                        SentToQueueOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StoreRepQueuedEmails");
        }
    }
}
